#include <stdio.h>

#include "neural_network_library.h"

#ifndef MNIST_DATA_HEADER
#define MNIST_DATA_HEADER

typedef struct {
	unsigned char *pixels; 

	unsigned int rows; 
	unsigned int columns;
	unsigned int num_examples;

	unsigned char *digits; 
} train_images;

train_images *read_train_images(char *train_images_path, char *train_numbers_path);
void free_train_images(train_images *images);
void copy_training_data(examples_container *container, train_images *images, unsigned int image_offset);
int read_little_endian(FILE *file);

#endif
