#ifndef NEURAL_NETWORK_LIBRARY_HEADER
#define NEURAL_NETWORK_LIBRARY_HEADER

#include <stdlib.h>

#include "utils.h"

typedef void (*act_function)(float_vector *dst, float_vector *values, uint_vector *indices);
typedef void (*act_function_deriv)(float_vector *dst, float_vector *values, uint_vector *indices);

typedef struct {
	float_vector **input; 
	float_vector **expected_output;

	size_t num_examples;
} examples_container;

typedef struct {
	sparse_matrix *weight_matrix; 
	float_vector *neuron_input;
	float_vector *activation;
	float_vector *error;
	float_vector *error_deriv;
	uint_vector **update_stages; 

	act_function afunc; 
	act_function_deriv afunc_deriv;

	size_t num_update_stages;
	size_t num_neurons;
} neural_network; 

// neural_network stuff
neural_network *create_feed_forward_network(size_t *layers, size_t num_layers);
void set_weight(neural_network *network, unsigned int source_neuron, unsigned int target_neuron, float value);
void zero_weight(neural_network *network, unsigned int source_neuron, unsigned int target_neuron); 
void copy_weights_from_targets_to_source(neural_network *network);
void free_neural_network(neural_network *network);
int step(neural_network *network, float_vector *input);
sparse_array *get_incoming_connections(neural_network *network, unsigned int neuron); 
sparse_array *get_outgoing_connections(neural_network *network, unsigned int neuron); 
float calculate_mse(neural_network *network, examples_container *container); 

// examples_container stuff
examples_container *create_examples_container(size_t input_size, size_t output_size, size_t num_examples);
void free_examples_container(examples_container *container); 

// math stuff

#endif
