#ifndef UTILS_HEADER
#define UTILS_HEADER

#define MIN(X, Y) X < Y ? X : Y 
#define MAX(X, Y) X > Y ? X : Y
#define ABS(X) X < 0 ? -X : X

#define BLOCK_SIZE	4
#define SSE_ALIGNMENT 	32

typedef struct {
	float *data; 
	size_t capacity;
} float_vector;

typedef struct {
	unsigned int *data; 
	size_t capacity;
} uint_vector;

typedef struct {
	float_vector *data; 
	uint_vector *indices;

	float *data_ptr; 
	unsigned int *indices_ptr;
	
	size_t capacity;
	size_t real_capacity;
	unsigned int index;
} sparse_array;

typedef struct {
	sparse_array **compressed_columns; 
	float **columns_data; //shortcut pointers
	unsigned int **columns_indices;

	sparse_array **compressed_rows;
	float **rows_data;
	unsigned int **rows_indices;

	uint_vector **column_index;

	size_t rows; 
	size_t columns;
	char look_up_index_correct;
} sparse_matrix;

//int & float_vector stuff
uint_vector *create_uint_vector(size_t capacity);
float_vector *create_float_vector(size_t capacity);

void resize_uint_vector(uint_vector *vector, size_t new_capacity); 
void resize_float_vector(float_vector *vector, size_t new_capacity); 
void free_uint_vector(uint_vector *vector); 
void free_float_vector(float_vector *vector);

// sparse array stuff
sparse_array *create_sparse_array(size_t real_capacity, size_t start_capacity); 
int set_entry(sparse_array *array, unsigned int index, float value); 
int look_up_index(sparse_array *array, unsigned int index); 
int zero_entry(sparse_array *array, unsigned int index); 
int resize_sparse_array(sparse_array *array, size_t new_capacity); 
void update_shortcut_pointers_array(sparse_array *array);
void free_sparse_array(sparse_array *array);

// sparse_matrix stuff 
sparse_matrix *create_sparse_matrix(size_t rows, size_t columns, size_t start_capacity);
int set_entry_matrix(sparse_matrix *matrix, unsigned int row, unsigned int column, float value);
int zero_entry_matrix(sparse_matrix *matrix, unsigned int row, unsigned int column);
void build_lookup_index(sparse_matrix *matrix);
void update_shortcut_pointers_matrix(sparse_matrix *matrix, unsigned int row, unsigned int column); 
void mult_vector_matrix_partial(float_vector *dst, float_vector *input, sparse_matrix *matrix, uint_vector *update_stage); 
void free_sparse_matrix(sparse_matrix *matrix);

float random_number(float min, float max); 
#endif
