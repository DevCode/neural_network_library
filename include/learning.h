#ifndef LEARNING_HEADER
#define LEARNING_HEADER

#include "neural_network_library.h"
#include "utils.h"

typedef struct {
	float_vector *error;
	float_vector *error_deriv; // needed for offline learning
	float_vector **delta_weights;
} backprop_data;

backprop_data *create_online_backprop_data(neural_network *network); 
backprop_data *create_offline_backprop_data(neural_network *network);

void free_backprop_data(backprop_data *bdata);

void backprop(neural_network *network, backprop_data *bdata, examples_container *container, float learning_rate);

typedef struct {
	float_vector *error; 
	float_vector *error_deriv;
	float_vector *previous_error;
	float_vector **delta_weights;
	float_vector **learning_rates;
} rprop_data;

rprop_data *create_rprop_data(neural_network *network); 
void free_rprop_data(rprop_data *rdata);

void rprop(neural_network *network, rprop_data *rdata, examples_container *container);

// backprop the error without updating any weights
void backprop_errors(neural_network *network, float_vector *input, float_vector *expected_output);

#endif
