#include <stdio.h>
#include <math.h>

#if defined(_OPENMP)
	#include <omp.h>
#endif

#include "mnist_data.h"
#include "neural_network_library.h"
#include "utils.h"
#include "learning.h"

#define PASSES 		10	
#define SAMPLES 	5	

void function(float_vector *dst, float_vector *src, uint_vector *indices);
void function_deriv(float_vector *dst, float_vector *src, uint_vector *indices);

__always_inline float sigmoid(float x) {
	return 1.0f / (1.0f + (float) exp(-x));
}

int main(int argc, char *argv[]) {
	setbuf(stdout, NULL);

	size_t container_size = 50000 / SAMPLES;
	train_images *images = read_train_images(argv[1], argv[2]);	
	examples_container *container = create_examples_container(images->rows * images->columns, 10, container_size);
	float learning_rate = 0.25f;

	size_t layers[3] = {images->rows * images->columns, 64, 10};
	neural_network *network = create_feed_forward_network(layers, 3);
	backprop_data *bdata = create_online_backprop_data(network);
	network->afunc = function; 
	network->afunc_deriv = function_deriv;
	
	for (int i = 0; i < PASSES; i++) {
		printf("Pass[%d/%d] \n", i + 1, PASSES);
		for (int j = 0; j < SAMPLES; j++) {
			copy_training_data(container, images, container->num_examples * j);
			backprop(network, bdata, container, learning_rate);

			printf("SubPass[%d/%d] \r", j + 1, SAMPLES);
		}
		printf("\n");
		learning_rate *= 0.9f;
	}

	examples_container *test_container = create_examples_container(images->rows * images->columns, 10, 60000 - SAMPLES * container_size);
	copy_training_data(test_container, images, container_size * SAMPLES);

	unsigned int errors = 0;
	for (unsigned int i = 0; i < test_container->num_examples; i++) {
		step(network, test_container->input[i]); 

		unsigned int *last_update_stage = network->update_stages[network->num_update_stages - 1]->data;
		size_t len = network->update_stages[network->num_update_stages - 1]->capacity; 

		int max_index = 0;
		float max_val = network->activation->data[last_update_stage[0]];
		int max_expected_index = 0; 
		float max_expected_val = test_container->expected_output[i]->data[0];
		for (unsigned int j = 0; j < len; j++) {
			if (network->activation->data[last_update_stage[j]] > max_val) {
				max_val = network->activation->data[last_update_stage[j]]; 
				max_index = j;
			}
			if (test_container->expected_output[i]->data[j] > max_expected_val) {
				max_expected_val = test_container->expected_output[i]->data[j]; 
				max_expected_index = j;
			}
		}
		if (max_expected_index != max_index) errors++; 

	}
	printf("[%d/%ld] Wrong \n", errors, test_container->num_examples);

	free_neural_network(network);
	free_train_images(images);	
	free_examples_container(container);
	free_examples_container(test_container);
	return 0;
}

void function(float_vector *dst, float_vector *src, uint_vector *indices) {
	for (unsigned int i = 0; i < indices->capacity; i++) {
		dst->data[indices->data[i]] = sigmoid(src->data[indices->data[i]]);
	}
}
void function_deriv(float_vector *dst, float_vector *src, uint_vector *indices) { 
	for (unsigned int i = 0; i < indices->capacity; i++) {
		float sig = sigmoid(src->data[indices->data[i]]);
		dst->data[indices->data[i]] = sig * (1.0f - sig);
	}
}
