#include <stdlib.h>
#include <x86intrin.h>

#if defined(_OPENMP)
	#include <omp.h>
#endif

#include "learning.h"
#include "neural_network_library.h"
#include "utils.h"

backprop_data *create_online_backprop_data(neural_network *network) {
	backprop_data *bdata = malloc(sizeof(backprop_data)); 
	bdata->error = create_float_vector(network->num_neurons); 
	bdata->error_deriv = create_float_vector(network->num_neurons);
	bdata->delta_weights = NULL;
	return bdata;
}

backprop_data *create_offline_backprop_data(neural_network *network) {
	backprop_data *bdata = malloc(sizeof(backprop_data)); 
	bdata->delta_weights = malloc(sizeof(float_vector *) * network->num_neurons);
	bdata->error = create_float_vector(network->num_neurons); 
	bdata->error_deriv = create_float_vector(network->num_neurons);
	
	for (unsigned int i = 0; i < network->num_neurons; i++) {
		bdata->delta_weights[i] = create_float_vector(get_outgoing_connections(network, i)->index); // how many outgoing connections has neuron[i]
	}

	return bdata;
}

void free_backprop_data(backprop_data *bdata) {
	free_float_vector(bdata->error); 
	free(bdata);
}

void backprop(neural_network *network, backprop_data *bdata, examples_container *container, float learning_rate) {
	unsigned int last_update_stage = network->num_update_stages - 1;
	float *error_data = bdata->error->data;
	float *error_deriv_data = bdata->error_deriv->data; 
	float *activation_data = network->activation->data;
	uint_vector *output_indices = network->update_stages[last_update_stage];

	if (bdata->delta_weights != NULL) {
		#pragma omp parallel for
		for (unsigned int i = 0; i < network->num_neurons; i++) {
			for (unsigned int j = 0; j < bdata->delta_weights[i]->capacity; j++) {
				bdata->delta_weights[i]->data[j] = 0;
			}
		}
	}
		
	for (unsigned int example = 0; example < container->num_examples; example++) {
		step(network, container->input[example]);

		network->afunc_deriv(bdata->error_deriv, network->neuron_input, output_indices);

		for (unsigned int i = 0; i < output_indices->capacity; i++) {
			unsigned int neuron_index = output_indices->data[i];

			error_data[neuron_index] = error_deriv_data[neuron_index] * (container->expected_output[example]->data[i] - activation_data[neuron_index]);
		}

		for (int i = last_update_stage - 1; i >= 0; i--) { // neuron in update stage

			unsigned int *update_stage = network->update_stages[i]->data;
			size_t len = network->update_stages[i]->capacity;

			if (i != 0) // input layer doesn't need derivative because the error doesn't get backpropagated -> no layers behind
				network->afunc_deriv(bdata->error_deriv, network->neuron_input, network->update_stages[i]);

			#pragma omp parallel for firstprivate(network, activation_data, update_stage)
			for (unsigned int j = 0; j < len; j++) { // neuron
				sparse_array *target_neurons = get_outgoing_connections(network, update_stage[j]);
				unsigned int *indices = target_neurons->indices_ptr;
				float *weights = target_neurons->data_ptr;
				float *delta_weights = NULL; 

				if (bdata->delta_weights != NULL)
					delta_weights = bdata->delta_weights[update_stage[j]]->data;

				float activation_times_learning_rate = activation_data[update_stage[j]] * learning_rate;
				float error = 0; 

				unsigned int rest_index = target_neurons->index & 3; // == % 4
				unsigned int len_sse = target_neurons->index - rest_index; // how long to use sse
				unsigned int rest_len = rest_index + len_sse;

				for (unsigned int k = 0; k != len_sse; k+=4) {
					__m128 weights_sse = _mm_load_ps(&weights[k]);
					__m128 activation_learning_sse = _mm_load_ps1(&activation_times_learning_rate);
					__m128 error_data_sse = _mm_setr_ps(
									error_data[indices[k]], 
									error_data[indices[k+1]],
									error_data[indices[k+2]], 
									error_data[indices[k+3]]);

					// weights += error_data[indices[k]] * activation_times_learning_rate
					__m128 delta_weight_sse = _mm_mul_ps(error_data_sse, activation_learning_sse); 
					__m128 new_weight_sse = _mm_add_ps(weights_sse, delta_weight_sse); 

					__m128 new_error_sse = _mm_mul_ps(new_weight_sse, error_data_sse);
				
					if (delta_weights == NULL) // if online learning update weight 
						_mm_store_ps(&weights[k], new_weight_sse);
					else 
						_mm_store_ps(&delta_weights[k], delta_weight_sse);

					float *erg_data_ptr = (float *) &new_error_sse;
					error += erg_data_ptr[0] + erg_data_ptr[1] + erg_data_ptr[2] + erg_data_ptr[3];
				}

				for (unsigned int k = len_sse; k < rest_len; k++) {
					float delta_weight = error_data[indices[k]] * activation_times_learning_rate;

					error += (weights[k] + delta_weight) * error_data[indices[k]]; 

					if (delta_weights == NULL)
						weights[k] += delta_weight;
					else 
						delta_weights[k] += delta_weight; 

				}

				error_data[update_stage[j]] = error_deriv_data[update_stage[j]] * error;
			}
		}
		if (bdata->delta_weights == NULL) 
			copy_weights_from_targets_to_source(network);  
	}

	if (bdata->delta_weights != NULL) {
		// update weights from temporary storage
		#pragma omp parallel for
		for (unsigned int i = 0; i < network->num_neurons; i++) {
			sparse_array *target_neurons = get_outgoing_connections(network, i); 
			float *data = target_neurons->data->data; 
			unsigned int len = target_neurons->index;
			
			for (unsigned int j = 0; j < len; j++) {
				data[j] += bdata->delta_weights[i]->data[j];
			}
		}
 		copy_weights_from_targets_to_source(network);  
	}
}

rprop_data *create_rprop_data(neural_network *network) {
	rprop_data *rdata = malloc(sizeof(rprop_data)); 
	rdata->error = create_float_vector(network->num_neurons); 
	rdata->error_deriv = create_float_vector(network->num_neurons);
	rdata->previous_error = create_float_vector(network->num_neurons);
	rdata->learning_rates = malloc(sizeof(sparse_array *) * network->num_neurons);
	rdata->delta_weights = malloc(sizeof(sparse_array *) * network->num_neurons);

	for (unsigned int i = 0; i < network->num_neurons; i++) {
		size_t outgoing_connections = get_outgoing_connections(network, i)->index;

		rdata->learning_rates[i] = create_float_vector(outgoing_connections);
		rdata->delta_weights[i] = create_float_vector(outgoing_connections);
	}

	return rdata;
}

void free_rprop_data(rprop_data *rdata) {
	for (unsigned int i = 0; i < rdata->error->capacity; i++) {
		free_float_vector(rdata->learning_rates[i]);
		free_float_vector(rdata->delta_weights[i]); 
	}

	free_float_vector(rdata->error); 
	free_float_vector(rdata->previous_error);
	free(rdata->learning_rates); 
	free(rdata);
}

// implemenent
void rprop(neural_network *network, rprop_data *rdata, examples_container *container) {

}

