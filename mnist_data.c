#include <stdio.h>
#include <stdlib.h>

#include "mnist_data.h"

train_images *read_train_images(char *train_images_path, char *train_numbers_path) {
	FILE *images_file = fopen(train_images_path, "rb"); 
	FILE *labels_file = fopen(train_numbers_path, "rb"); 
	
	if (images_file == NULL || labels_file == NULL) {
		printf("Error opening files ... \n"); 
		return NULL;
	}

	/*int magic_number_images = */read_little_endian(images_file); // maybe check magic numbers
	/*int magic_number_labels = */read_little_endian(labels_file); 

	unsigned int num_examples = read_little_endian(images_file); 

	if (num_examples != (unsigned int) read_little_endian(labels_file)) { 
		printf("Error : number of images != number of labels .. \n"); 
		return NULL;
	}

	train_images *images = malloc(sizeof(train_images)); 
	images->num_examples = num_examples;

	images->rows = read_little_endian(images_file); 
	images->columns = read_little_endian(images_file);

	size_t image_size = images->rows * images->columns;

	images->pixels = malloc(image_size * images->num_examples);
	images->digits = malloc(images->num_examples);

	unsigned int off = 0;
	
	for (unsigned int i = 0; i < images->num_examples; i++) {
		size_t read = fread(&images->pixels[off], 1, image_size, images_file); 

		off += read;

		images->digits[i] = fgetc(labels_file);
	}

	printf("image_size[%d, %d] \n", images->rows, images->columns);

	fclose(images_file); 
	fclose(labels_file);
	return images;
}

void copy_training_data(examples_container *container, train_images *images, unsigned int image_offset) {
	size_t image_size = images->rows * images->columns;
	size_t output_size = container->expected_output[0]->capacity;
	size_t input_size = container->input[0]->capacity;

	#pragma omp parallel for firstprivate(container, images, image_offset, image_size, output_size, input_size)
	for (unsigned int i = 0; i < container->num_examples; i++) {

		for (unsigned int j = 0; j < input_size; j++) {
			container->input[i]->data[j] = images->pixels[(image_size * (i + image_offset)) + j] / 255.0f;	
		}

		for (unsigned int j = 0; j < output_size; j++) {
			if (j != images->digits[image_offset + i]) container->expected_output[i]->data[j] = 0;
			else container->expected_output[i]->data[j] = 1;
		}
	}
}

void free_train_images(train_images *images) {
	free(images->pixels); 
	free(images->digits);
	free(images);
}

int read_little_endian(FILE *file) { // check for errors ?  
	return (int) (fgetc(file) << 24 | fgetc(file) << 16 | fgetc(file) << 8 | fgetc(file));
}
