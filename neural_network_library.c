#include <stdlib.h>
#include <time.h>

#include <jemalloc/jemalloc.h>

#include <x86intrin.h>

#if defined(_OPENMP)
	#include <omp.h>
#endif

#include "neural_network_library.h"
#include "utils.h"

neural_network *create_feed_forward_network(size_t *layers, size_t num_layers) {
	srand(time(NULL)); // initialize random number generator

	neural_network *network = malloc(sizeof(neural_network)); 
	network->update_stages = malloc(sizeof(uint_vector *) * num_layers); 
	size_t num_neurons = 0; 
	unsigned int neuron = 0;

	for (unsigned int i = 0; i < num_layers; i++) {
		network->update_stages[i] = create_uint_vector(layers[i]);
		num_neurons += layers[i];	

		uint_vector *update_stage = network->update_stages[i];
		for (unsigned int j = 0; j < layers[i]; j++) {
			update_stage->data[j] = neuron++;
		}
	}

	network->num_neurons = num_neurons; 
	network->num_update_stages = num_layers;

	network->weight_matrix = create_sparse_matrix(num_neurons, num_neurons, 2); 
	network->neuron_input = create_float_vector(num_neurons); 
	network->activation = create_float_vector(num_neurons); 

	for (unsigned int i = 1; i < num_layers; i++) {
		unsigned int *layer = network->update_stages[i]->data;
		unsigned int *prev_layer = network->update_stages[i - 1]->data;
		
		for (unsigned int j = 0; j < network->update_stages[i - 1]->capacity; j++) {
			for (unsigned int k = 0; k < network->update_stages[i]->capacity; k++) {
				set_weight(network, prev_layer[j], layer[k], random_number(-0.5f, 0.5f));
			}
		}
	}
	return network;
}

__always_inline void set_weight(neural_network *network, unsigned int source_neuron, unsigned int target_neuron, float value) {
	set_entry_matrix(network->weight_matrix, target_neuron, source_neuron, value);
}

__always_inline void zero_weight(neural_network *network, unsigned int source_neuron, unsigned int target_neuron) {
	zero_entry_matrix(network->weight_matrix, target_neuron, source_neuron);
}

// bounds check
__always_inline sparse_array *get_incoming_connections(neural_network *network, unsigned int neuron) {
	return network->weight_matrix->compressed_columns[neuron];	
}

// bounds check
__always_inline sparse_array *get_outgoing_connections(neural_network *network, unsigned int neuron) {
	return network->weight_matrix->compressed_rows[neuron];
}

// use local pointers for faster access ?
int step(neural_network *network, float_vector *input) {
	if (input->capacity > network->update_stages[0]->capacity) return -1;

	for (unsigned int i = 0; i < network->update_stages[0]->capacity; i++) {
		network->activation->data[network->update_stages[0]->data[i]] = input->data[i];
	}

	for (unsigned int i = 1; i < network->num_update_stages; i++) {
		mult_vector_matrix_partial(network->neuron_input, network->activation, network->weight_matrix, network->update_stages[i]);
		network->afunc(network->activation, network->neuron_input, network->update_stages[i]); 
	}
	return 0;
}

void free_neural_network(neural_network *network) {
	for (unsigned int i = 0; i < network->num_update_stages; i++) {
		free_uint_vector(network->update_stages[i]);
	}

	free(network->update_stages); 
	free_float_vector(network->neuron_input); 
	free_float_vector(network->activation);
	free_sparse_matrix(network->weight_matrix);
	free(network);
}

examples_container *create_examples_container(size_t input_size, size_t output_size, size_t num_examples) {
	examples_container *container = malloc(sizeof(examples_container)); 
	container->input = malloc(sizeof(float_vector *) * num_examples); 
	container->expected_output = malloc(sizeof(float_vector *) * num_examples); 
	container->num_examples = num_examples;

	for (unsigned int i = 0; i < num_examples; i++) {
		container->input[i] = create_float_vector(input_size); 
		container->expected_output[i] = create_float_vector(output_size);
	}

	return container;
}

void free_examples_container(examples_container *container) {
	for (unsigned int i = 0; i < container->num_examples; i++) {
		free_float_vector(container->input[i]);
		free_float_vector(container->expected_output[i]);
	}

	free(container->input);
	free(container->expected_output);
	free(container);
}

float calculate_mse(neural_network *network, examples_container *container) {
	float error = 0;
	uint_vector *output_indices = network->update_stages[network->num_update_stages - 1];
	float *activation_data = network->activation->data;
	
	for (unsigned int example = 0; example < container->num_examples; example++) {
		step(network, container->input[example]);

		float specific_error = 0;

		for (unsigned int i = 0; i < output_indices->capacity; i++) {
			unsigned int neuron_index = output_indices->data[i];

			float delta_output = container->expected_output[example]->data[i] - activation_data[neuron_index];	

			specific_error += (delta_output * delta_output);
		}
		error += specific_error;
	}
	return 0.5f * error;
}

void copy_weights_from_targets_to_source(neural_network *network) {
	if (network->weight_matrix->look_up_index_correct == 0) 
		build_lookup_index(network->weight_matrix);

	float **compressed_columns_data = network->weight_matrix->columns_data;
	
	#pragma omp parallel firstprivate(compressed_columns_data, network) 
	for (unsigned int i = 0; i < network->num_update_stages; i++) { 
		unsigned int *update_stage = network->update_stages[i]->data;
		size_t len = network->update_stages[i]->capacity;
		size_t rest = len % BLOCK_SIZE;
		size_t len_block_updates = len - rest;

		#pragma omp for 
		for (unsigned int j = 0; j < len_block_updates; j+=BLOCK_SIZE) { // 8 neurons at a time
			sparse_array *neuron[BLOCK_SIZE]; 
			unsigned int *indices[BLOCK_SIZE];
			unsigned int *column_indices[BLOCK_SIZE];
			float *weights[BLOCK_SIZE];

			unsigned int min_len = ~0;
			unsigned int max_len = 0;

			for (unsigned int k = 0; k < BLOCK_SIZE; k++) {
				neuron[k] = get_outgoing_connections(network, update_stage[j + k]);
				indices[k] = neuron[k]->indices_ptr;
				weights[k] = neuron[k]->data_ptr;
				column_indices[k] = network->weight_matrix->column_index[j + k]->data;

				if (min_len > neuron[k]->index) min_len = neuron[k]->index;
				if (max_len < neuron[k]->index) max_len = neuron[k]->index;
			}
			
			for (unsigned int k = 0; k < min_len; k++) { 
				for (unsigned int l = 0; l < BLOCK_SIZE; l++) {
					unsigned int index = indices[l][k];
					unsigned int col_index = column_indices[l][k];

					compressed_columns_data[index][col_index] = weights[l][k];
				}
			}

			// could cause errors 
			for (unsigned int k = min_len; k < max_len; k++) {
				for (unsigned int l = 0; l < BLOCK_SIZE; l++) {
					if (neuron[l]->index > k) {
						unsigned int index = indices[l][k];
						unsigned int col_index = column_indices[l][k];

						compressed_columns_data[index][col_index] = weights[l][k];
					}
				}
			}
		}	

		#pragma omp for 
		for (unsigned int j = len_block_updates; j < len; j++) {
			sparse_array *neuron = get_outgoing_connections(network, update_stage[j]);
			unsigned int *indices = neuron->indices_ptr;
			unsigned int *column_indices = network->weight_matrix->column_index[j]->data;
			float *weights = neuron->data_ptr;

			for (unsigned int k = 0; k < neuron->index; k++) { 
				unsigned int index = indices[k];
				unsigned int col_index = column_indices[k];

				compressed_columns_data[index][col_index] = weights[k];
			}
		}
	}
}
