CC=gcc
SOURCES=main.c neural_network_library.c utils.c learning.c mnist_data.c
CFLAGS=-Iinclude -Wall -mfpmath=sse -ffast-math -msse4 -fopenmp -funroll-loops -O3 -march=native -g
LDFLAGS=-lm -ljemalloc
EXECUTABLE=neural_network

all :
	$(CC) $(SOURCES) $(CFLAGS) $(LDFLAGS) -o $(EXECUTABLE)


