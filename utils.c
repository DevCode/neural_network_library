#include <assert.h>
#include <string.h>
#include <stdlib.h>

#include <jemalloc/jemalloc.h>

#include <x86intrin.h>

#if defined(_OPENMP)
	#include <omp.h>
#endif

#include "utils.h"

const float RAND_FLOAT_MAX = (float) RAND_MAX;

uint_vector *create_uint_vector(size_t capacity) {
	uint_vector *vector = malloc(sizeof(uint_vector)); 
	vector->data = malloc(sizeof(int) * capacity); 
	vector->capacity = capacity; 

	for (unsigned int i = 0; i < capacity; i++) vector->data[i] = 0;

	return vector;
}

float_vector *create_float_vector(size_t capacity) {
	float_vector *vector = malloc(sizeof(float_vector)); 
	vector->data = malloc(sizeof(float) * capacity); 
	vector->capacity = capacity; 

	for (unsigned int i = 0; i < capacity; i++) vector->data[i] = 0.0f;

	return vector;
}

void resize_uint_vector(uint_vector *vector, size_t new_capacity) {
	unsigned int *new_data = realloc(vector->data, new_capacity * sizeof(int)); 

	assert(new_data != NULL);

	for (unsigned int i = vector->capacity; i < new_capacity; i++) new_data[i] = 0;

	vector->data = new_data; 
	vector->capacity = new_capacity;
}

void resize_float_vector(float_vector *vector, size_t new_capacity) {
	float *new_data = realloc(vector->data, new_capacity * sizeof(float)); 

	assert(new_data != NULL);

	for (unsigned int i = vector->capacity; i < new_capacity; i++) new_data[i] = 0;

	vector->data = new_data; 
	vector->capacity = new_capacity;
}

void free_uint_vector(uint_vector *vector) {
	free(vector->data); 
	free(vector);
}

void free_float_vector(float_vector *vector) {
	free(vector->data); 
	free(vector);
}

sparse_array *create_sparse_array(size_t real_capacity, size_t start_capacity) {
	sparse_array *arr = malloc(sizeof(sparse_array)); 

	arr->data = create_float_vector(start_capacity); 
	arr->indices = create_uint_vector(start_capacity); 
	arr->data_ptr = arr->data->data;
	arr->indices_ptr = arr->indices->data;

	arr->capacity = start_capacity; 
	arr->real_capacity = real_capacity; 
	arr->index = 0; 

	for (unsigned int i = 0; i < arr->capacity; i++) {
		arr->indices->data[i] = ~0;
	}

	return arr;
}

int look_up_index(sparse_array *array, unsigned int index) {
	unsigned int *indices = array->indices->data;

	int middle = 0; 
	int left = 0; 
	int right = array->index;

	while(left <= right) {
		middle = left + ((right - left) >> 1); 
		if (indices[middle] != index) {
			if (indices[middle] > index) right = middle - 1; 
			else left = middle + 1;
		} else return middle;
	}
	
	return -middle;
}

int set_entry(sparse_array *array, unsigned int index, float value) {
	if (index >= array->real_capacity) return -1; 

	int ret = look_up_index(array, index); 
	int pos = ABS(ret);
	
	if (ret <= 0 && array->indices->data[pos] != index) {
		pos = ABS(pos);
		if (array->index + 1 >= array->capacity) {
			size_t new_capacity = MIN(array->capacity * 2, array->real_capacity);

			resize_uint_vector(array->indices, new_capacity); 
			resize_float_vector(array->data, new_capacity);
			
			array->capacity = new_capacity;
		}
		int len = MAX(array->index - pos, 0);
		
		memmove(&array->indices->data[pos+1], &array->indices->data[pos], sizeof(int) * len);	
		memmove(&array->data->data[pos+1], &array->data->data[pos], sizeof(float) * len);	
		array->indices->data[pos] = index; 
		array->index++;
	}
	array->data->data[pos] = value;
	update_shortcut_pointers_array(array);
	return ret;
}

int zero_entry(sparse_array *array, unsigned int index) {
	if (index >= array->real_capacity) return -1; 

	int pos = look_up_index(array, index); 

	if (pos >= 0) {
		int len = array->index - pos;
		memmove(&array->indices->data[pos], &array->indices->data[pos+1], sizeof(int) * len);	
		memmove(&array->data->data[pos], &array->data->data[pos+1], sizeof(float) * len);	
		array->index--;
		return pos;
	}
	return -1;
}

__always_inline void update_shortcut_pointers_array(sparse_array *array) {
	array->data_ptr = array->data->data;
	array->indices_ptr = array->indices->data;

}

void free_sparse_array(sparse_array *array) {
	free_float_vector(array->data); 
	free_uint_vector(array->indices); 
	free(array);
}

sparse_matrix *create_sparse_matrix(size_t rows, size_t columns, size_t start_capacity) {
	sparse_matrix *matrix = malloc(sizeof(sparse_matrix)); 
	matrix->compressed_columns = malloc(sizeof(sparse_array *) * rows); 
	matrix->compressed_rows = malloc(sizeof(sparse_array *) * columns); 
	matrix->column_index = malloc(sizeof(uint_vector *) * columns);

	matrix->rows_data = malloc(sizeof(float *) * columns); 
	matrix->rows_indices = malloc(sizeof(unsigned int *) * columns);

	matrix->columns_data = malloc(sizeof(float *) * rows); 
	matrix->columns_indices = malloc(sizeof(unsigned int *) * rows);
	matrix->look_up_index_correct = 0;

	for (unsigned int i = 0; i < rows; i++) {
		matrix->compressed_columns[i] = create_sparse_array(columns, start_capacity);  
	}

	for (unsigned int i = 0; i < columns; i++) {
		matrix->compressed_rows[i] = create_sparse_array(columns, start_capacity);
		matrix->column_index[i] = create_uint_vector(start_capacity);
	}

	matrix->rows = rows; 
	matrix->columns = columns;

	return matrix;
}

int set_entry_matrix(sparse_matrix *matrix, unsigned int row, unsigned int column, float value) {
	if (row >= matrix->rows || column >= matrix->columns) return -1; 

	int ret = set_entry(matrix->compressed_columns[row], column, value); 
	set_entry(matrix->compressed_rows[column], row, value);

	update_shortcut_pointers_matrix(matrix, row, column);

	if (ret < 0) matrix->look_up_index_correct = 0;

	return 0;
}



int zero_entry_matrix(sparse_matrix *matrix, unsigned int row, unsigned int column) {
	if (row >= matrix->rows || column >= matrix->columns) return -1;

	int ret = zero_entry(matrix->compressed_columns[row], column); 
	zero_entry(matrix->compressed_rows[column], row);

	update_shortcut_pointers_matrix(matrix, row, column);

	if (ret >= 0) matrix->look_up_index_correct = 0;
	return 0;
}

__always_inline void update_shortcut_pointers_matrix(sparse_matrix *matrix, unsigned int row, unsigned int column) {
	matrix->rows_data[column] = matrix->compressed_rows[column]->data->data;
	matrix->rows_indices[column] = matrix->compressed_rows[column]->indices->data;

	matrix->columns_data[row] = matrix->compressed_columns[row]->data->data; 
	matrix->columns_indices[row] = matrix->compressed_columns[row]->indices->data;
}

void mult_vector_matrix_partial(float_vector *dst, float_vector *input, sparse_matrix *matrix, uint_vector *update_stage) {

	float *input_ptr = input->data; 
	float *output_ptr = dst->data;
	#pragma omp parallel for 
	for (unsigned int i = 0; i < update_stage->capacity; i++) {
		
		sparse_array *row = matrix->compressed_columns[update_stage->data[i]];
		float *elements = row->data_ptr;
		unsigned int *indices = row->indices_ptr;
		unsigned int index = row->index;
		unsigned int rest = index & 3; // == % 4
		unsigned int len_sse = index - rest;

		float erg = 0;
		for (unsigned int j = 0; j != len_sse; j+=4) {
			__m128 data = _mm_load_ps(&elements[j]);
			__m128 input_data = _mm_setr_ps(input_ptr[indices[j]], input_ptr[indices[j + 1]], input_ptr[indices[j + 2]], input_ptr[indices[j + 3]]); 
			__m128 mul_erg = _mm_mul_ps(data, input_data);

			float *erg_arr = (float *)  &mul_erg;

			erg += erg_arr[0] + erg_arr[1] + erg_arr[2] + erg_arr[3];
		}

		for (unsigned int j = len_sse; j < index; j++) {
			erg += elements[j] * input->data[indices[j]];
		}

		output_ptr[update_stage->data[i]] = erg;
	}
}

void build_lookup_index(sparse_matrix *matrix) {
	#pragma omp parallel for
	for (unsigned int i = 0; i < matrix->columns; i++) { // column
		uint_vector *indices = matrix->column_index[i];
		sparse_array *row = matrix->compressed_rows[i];

		if (row->index >= indices->capacity) 
			resize_uint_vector(indices, MIN(row->index * 2, matrix->rows));
		
		for (unsigned int j = 0; j < row->index; j++) { // row
			sparse_array *column = matrix->compressed_columns[row->indices_ptr[j]];
			indices->data[j] = ABS(look_up_index(column, i));
		}
	}
	matrix->look_up_index_correct = 1;
}

void free_sparse_matrix(sparse_matrix *matrix) {
	for (unsigned int i = 0; i < matrix->rows; i++) {
		free_sparse_array(matrix->compressed_columns[i]); 
	}

	for (unsigned int i = 0; i < matrix->columns; i++) {
		free_sparse_array(matrix->compressed_rows[i]); 
		free_uint_vector(matrix->column_index[i]);
	}
	free(matrix->compressed_rows); 
	free(matrix->column_index);
	free(matrix->compressed_columns);
	free(matrix); 
}

float random_number(float min, float max) {
	return (rand() / RAND_FLOAT_MAX) * (max - min) + min;
}
